extern crate portaudio;
extern crate hound;
extern crate rustfft;
extern crate num;

use portaudio as pa;
use std::f64::consts::PI;

const CHANNELS: i32 = 2;
const NUM_SECONDS: i32 = 30;
const SAMPLE_RATE: f64 = 44_100.0;
const FRAMES_PER_BUFFER: u32 = 64;
const TABLE_SIZE: usize = 100;
const WINDOW_SIZE: usize = 16385;
const HAMMING_ALPHA: f64 = 0.53836; //wikipedia'd
const HAMMING_BETA: f64 = 0.46164; //wikipedia'd

type SigSample = num::Complex<f64>; // Samples in the intermediate signal are complex because of FT
type Signal = Vec<SigSample>;

fn main() {
    run().unwrap()
}

fn sawtooth(sawtooth_period: f64, num_samples: usize) -> Signal {
    let mut sawtooth_sig: Signal = Vec::new();
    for i in 0..num_samples {
        sawtooth_sig.push(num::Complex {re: (((((i as f64) / sawtooth_period) % 1.0) * 2.0 - 1.0) as f64),
                                        im: 0.0});
    }
    sawtooth_sig
}

fn constant_signal(start: SigSample, num_samples: usize) -> Signal {
    let mut result: Signal = Vec::new();
    for i in 0..num_samples {
        result.push(start);
    }
    result
}

fn linear_signal(start: SigSample, end: SigSample, num_samples: usize) -> Signal {
    let mut result: Signal = Vec::new();
    for i in 0..num_samples {
        let time_fraction = (i as f64) / (num_samples as f64);
        result.push(time_fraction * end + (1.0 - time_fraction) * start);
    }
    result
}

fn sine_signal(amplitude: &Signal, frequency: &Signal, num_samples: usize) -> Signal {
    let mut carrier: Signal = Vec::new();
    for i in 0..num_samples {
        let phase_fraction = (i as f64) * frequency[i] / (SAMPLE_RATE);
        carrier.push((phase_fraction * PI* 2.0).sin() * amplitude[i]);
    }
    carrier
}

fn add_signals(sig1: &Signal, sig2: &Signal) -> Signal {
    let len = std::cmp::min(sig1.len(), sig2.len());
    let mut result = Vec::with_capacity(len);
    for i in 0..len {
        result.push(sig1[i] + sig2[i]);
    }
    result
}

// a sine wave whose frequency, by which I mean phase, linearly changes
fn shifting_sine(start_period: f64, end_period: f64, amplitude:f64, num_samples: usize) -> Signal {
    let mut wave: Signal = Vec::with_capacity(num_samples);
    let samples_f = num_samples as  f64;
    let period_shift = end_period - start_period;
    for i in 0..num_samples {
        let time_fraction = (i as f64) / (num_samples as f64);
        let period = start_period + period_shift * time_fraction;
        let phase_fraction = (i as f64) / period;
        wave.push(num::Complex{re: ((phase_fraction * PI * 2.0).sin() as f64) * amplitude, im: 0.0});
    }
    wave
}
        
fn sawtooth_signal(amplitude: &Signal, frequency: &Signal, num_samples: usize) -> Signal {
    let mut sawtooth_sig: Signal = Vec::new();
    for i in 0..num_samples {
        let phase_fraction = (i as f64) * frequency[i].re / (SAMPLE_RATE);
        sawtooth_sig.push(amplitude[i] * (phase_fraction % 1.0));
    }
    sawtooth_sig
}

fn square_signal(amplitude: &Signal, frequency: &Signal, num_samples: usize) -> Signal {
    let mut square_sig: Signal = Vec::new();
    for i in 0..num_samples {
        let phase_fraction = (i as f64) * frequency[i].re / (SAMPLE_RATE);
        let square = if phase_fraction > 0.5 {
            1.0
        } else {
            -1.0
        };
        square_sig.push(amplitude[i] * square);
    }
    square_sig
}

fn sine(amplitude: &Signal, carrier_period: f64, num_samples: usize) -> Signal {
    sine_signal(amplitude, &constant_signal(num::Complex{re: SAMPLE_RATE/carrier_period, im: 0.0}, num_samples), num_samples)
}

fn ramped_sine(start_amplitude: f64, end_amplitude: f64, carrier_period: f64, num_samples: usize) -> Signal {
    sine(&linear_signal(num::Complex{re: start_amplitude, im: 0.0}, num::Complex{re: end_amplitude, im: 0.0}, num_samples), 
         carrier_period,
         num_samples)
}

fn fm_sine(carrier_period: f64, num_samples: usize, modulator_sig: &Signal) -> Signal {
    let mut carrier: Signal = Vec::new();
    for i in 0..num_samples {
        let phase_fraction = (i as f64) / (carrier_period as f64);
        let new_phase = phase_fraction + modulator_sig[i].re;
        carrier.push(num::Complex{re: (new_phase * PI * 2.0).sin(),
                                  im: 0.0});
    }
    carrier
}

fn ampl_adsr(atk_samples: usize, dec_samples: usize, rel_samples: usize, sus_ampl: f64, total_samples: usize) -> Signal {
    // could also not assume start and end at 0, attack to 1
    let sus_samples = total_samples - atk_samples - dec_samples - rel_samples; // could just take this as param instead?
    let mut result = linear_signal(num::Complex{re: 0.0, im: 0.0}, num::Complex {re: 1.0, im: 0.0}, atk_samples);
    result.append(&mut linear_signal(num::Complex{re: 1.0, im: 0.0}, num::Complex {re: sus_ampl, im:0.0}, dec_samples));
    result.append(&mut constant_signal(num::Complex{re: sus_ampl, im:0.0}, sus_samples));
    result.append(&mut linear_signal(num::Complex{re: sus_ampl, im: 0.0}, num::Complex{re: 0.0, im: 0.0}, rel_samples));
    result
}


//shown below: Tim doesn't actually know what phase is or how it relates to frequency

// fn freq_envelope(atk_samples: usize, dec_samples: usize, sus_samples: usize, rel_samples: usize,
//                  init_period: f64, atk_period: f64, sus_period: f64, final_period: f64, amplitude: f64) -> Signal {
//     let mut wave: Signal = Vec::with_capacity(atk_samples + dec_samples + sus_samples + rel_samples);
//     let asF = atk_samples as f64;
//     let dsF = dec_samples as f64;
//     let rsF = rel_samples as f64;

//     let atk_pshift = atk_period - init_period;
//     let dec_pshift = sus_period - atk_period;
//     let rel_pshift = final_period - sus_period;

//     let sec1 = atk_samples;
//     let sec2 = sec1 + dec_samples;
//     let sec3 = sec2 + sus_samples;
//     let sec4 = sec3 + rel_samples;
//     println!("ATTACK--------------------------------------------------------");
//     let mut ppf = 0.0;
//     let mut smp = 0;
//     for i in 0..atk_samples {
//         let phase_fraction = (smp as f64) / (init_period + (atk_pshift * (i as f64) / asF));
//         println!("{}", phase_fraction - ppf);
//         wave.push(num::Complex{re: ((phase_fraction * PI * 2.0).sin() as f64) * amplitude, im: 0.0});
//         smp += 1;
//         ppf = phase_fraction;
//     }

//     println!("DECAY--------------------------------------------------------");
//     for i in 0..dec_samples {
//         let phase_fraction = (smp as f64) / (atk_period + (dec_pshift * (i as f64) / dsF));
//         println!("{}", phase_fraction - ppf);
//         wave.push(num::Complex{re: ((phase_fraction * PI * 2.0).sin() as f64) * amplitude, im: 0.0});
//         smp += 1;
//         ppf = phase_fraction;
//     }

//     println!("SUSTAIN--------------------------------------------------------");
//     for i in sec2..sec3 {
//         let phase_fraction = (i as f64) / sus_period;
//         println!("{}", phase_fraction - ppf);
//         wave.push(num::Complex{re: ((phase_fraction * PI * 2.0).sin() as f64) * amplitude, im: 0.0});
//         smp += 1;
//         ppf = phase_fraction;
//     }

//     println!("RELEASE--------------------------------------------------------");
//     for i in 0..rel_samples {
//         let phase_fraction = (smp as f64) / (sus_period + rel_pshift * (i as f64) / rsF);
//         println!("{}", phase_fraction - ppf);
//         wave.push(num::Complex{re: ((phase_fraction * PI * 2.0).sin() as f64) * amplitude, im: 0.0});
//         smp += 1;
//         ppf = phase_fraction;
//     }

//     wave
// }
   
fn zeroes(num_samples: usize) -> Signal {
    vec![num::Complex{re: 0.0, im: 0.0}; num_samples]
}

fn read_signals_from_wav(filename: &str, bits: u32, length: Option<usize>) -> (Signal, Signal) {
    let max_sample: u64 = (2 as u64).pow(bits);
    let mut reader = hound::WavReader::open(filename).unwrap();
    let samples: Vec<i32> = reader.samples().map(|s| s.unwrap()).collect();

    let mut left_samples: Vec<i32> = Vec::new();
    let mut right_samples: Vec<i32> = Vec::new();
    println!("File is {} samples", samples.len());
    let num_samples = match length {
        None => samples.len(),
        Some(a) => a,
    };
    for i in 0..num_samples {
        if i%2 == 0 {
            left_samples.push(samples[i]);
        } else {
            right_samples.push(samples[i]);
        }
    }
    // Convert from <bits> bit signed PCM to f64 in [-1, 1].
    let left_wav_sig: Vec<num::Complex<_>> = left_samples.iter().map(|&s| num::Complex{re: ((s as f64)/(max_sample as f64)), im: 0.0}).collect();
    let right_wav_sig: Vec<num::Complex<_>> = right_samples.iter().map(|&s| num::Complex{re: ((s as f64)/(max_sample as f64)), im: 0.0}).collect();
    (left_wav_sig, right_wav_sig)
}

fn write_wav_from_signals(filename: &str, left: &Signal, right: &Signal) -> () {
    let hound_spec = hound::WavSpec {
        channels: 2,
        sample_rate: 44_100,
        bits_per_sample: 32
    };
    let mut writer = hound::WavWriter::create(filename, hound_spec).unwrap();
    for i in 0..left.len() {
        let left_sample = (left[i].re * (std::i32::MAX as f64)) as i32;
        let right_sample = (right[i].re * (std::i32::MAX as f64)) as i32;
        writer.write_sample(left_sample).unwrap();
        writer.write_sample(right_sample).unwrap();
    }
    writer.finalize().unwrap();
}

fn stft(sig: &Signal, window: [f64; WINDOW_SIZE], window_jump: usize, fft_len: usize) -> Vec<Signal> {
    let mut fft: rustfft::FFT<f64> = rustfft::FFT::new(fft_len, false);
    let mut window_start = 0;

    let mut spec: Vec<Signal> = Vec::new();
    //forward transform
    while window_start < (sig.len() - WINDOW_SIZE) {
        let mut window_signal = vec![num::Complex{re: 0.0, im: 0.0}; fft_len];
        for i in 0..WINDOW_SIZE {
            window_signal[i] = sig[window_start + i] * window[i];
        }
        let mut window_spectrum = window_signal.clone();
        fft.process(&window_signal, &mut window_spectrum);
        spec.push(window_spectrum);
        window_start += window_jump;
    }
    spec
}

fn modulate_amplitude(carrier: &Signal, amplitude: &Signal) -> Signal {
    let len = std::cmp::min(carrier.len(), amplitude.len());
    let mut result = Vec::with_capacity(len);
    for i in 0..len {
        result.push(carrier[i] * amplitude[i]);
    }
    result
}

fn reverse_stft(spec: &Vec<Signal>, fft_len: usize) -> Vec<Signal> {
    let mut reverse_fft: rustfft::FFT<f64> = rustfft::FFT::new(fft_len, true);
    let mut transformed_sigs: Vec<Vec<num::Complex<_>>> = Vec::new();
    for i in 0..spec.len() {
        let mut transformed_sig = spec[i].clone();
        reverse_fft.process(&spec[i], &mut transformed_sig);
        for j in 0..transformed_sig.len() {
            let temp = transformed_sig[j];
            transformed_sig[j] = temp / (WINDOW_SIZE as f64);
        }
        transformed_sigs.push(transformed_sig);
    }
    transformed_sigs
}

fn scale_channel(channel: &mut Signal, scale: f64) -> () {
    for i in 0..channel.len() {
        channel[i] = channel[i] * scale;
    }
}

fn renormalize_track(left: &mut Signal, right: &mut Signal) -> () {
    //find max
    //FIXME: only checks positive samples
    let mut max_amplitude: f64 = 0.0;
    for s in left.iter() {
        if s.re > max_amplitude {
            max_amplitude = s.re;
        }
    }
    for s in right.iter() {
        if s.re > max_amplitude {
            max_amplitude = s.re;
        }
    }
    let scale = 1.0/max_amplitude;
    scale_channel(left, scale);
    scale_channel(right, scale);
}

fn run() -> Result<(), pa::Error> {
    println!("PortAudio Test. Sample Rate = {}, Bufsize = {}", SAMPLE_RATE, FRAMES_PER_BUFFER);

    ///////////////////////////////////
    // Generate Hamming Window
    ///////////////////////////////////
    println!("Generate Hamming Window");
    let mut window = [0.0; WINDOW_SIZE]; //f64s
    for i in 0..WINDOW_SIZE {
        window[i] = HAMMING_ALPHA - (HAMMING_BETA * ((2.0*PI*(i as f64))/(WINDOW_SIZE as f64 -1.0)).cos());
        //let half_length: f64 = ((WINDOW_SIZE - 1) as f64) / 2.0;
        //window[i] = 1.0 - ((i as f64 - half_length)/half_length).abs();
    }
    window[0] = window[0]/2.0;
    window[WINDOW_SIZE-1] = window[WINDOW_SIZE-1]/2.0;

    ///////////////////////////////////
    // Read .WAV File
    ///////////////////////////////////
    println!("Read .WAV File");

    //part 1
    //let (left_wav_sig, right_wav_sig) = read_signals_from_wav("../3.1-sounds/Rust_maid_kernel.wav", 16, None);
    //part 2
    //let (left_wav_sig, right_wav_sig) = read_signals_from_wav("../3.1-sounds/Clanky_kernel_redux.wav", 16, None);
    //part 3
    let (left_wav_sig, right_wav_sig) = read_signals_from_wav("kernel.wav", 24, Some(22_050));
    let duration0 = 2 * SAMPLE_RATE as usize;
    let duration1 = 10 * SAMPLE_RATE as usize;
    let per0 = 100.0;
    let per1 = 200.0;
    let ratio0 = 0.67;
    let ratio1 = 2.0;

    let mut sigs = Vec::new();

    let const1 = &constant_signal(num::Complex{re: 1.0, im: 0.0}, duration1);
    
    let envelope1 = ampl_adsr((SAMPLE_RATE * 0.2) as usize,
                              (SAMPLE_RATE * 0.8) as usize,
                              (SAMPLE_RATE) as usize,
                              0.5,
                              duration1);

    sigs.push(sine_signal(&const1,&
                          linear_signal(num::Complex{re:800.0, im:0.0}, num::Complex{re:100.0, im:0.0}, duration1),
                          duration1));
        
    
    //sigs.push(fm_sine(200.0, duration1, &sine(&envelope1, 300.0, duration1)));

    // imitation FM with PM
    let lfo2hz2amp = sine(&constant_signal(num::Complex{re: 2.0, im:0.0}, duration1), SAMPLE_RATE / 2.0, duration1);
    let lfo2hz2amp_enveloped = modulate_amplitude(&lfo2hz2amp,  &envelope1);
    //sigs.push(fm_sine(200.0, duration1, &lfo2hz2amp_enveloped));
    
    // real FM
    let lfo2hz40amp = sine(&constant_signal(num::Complex{re: 40.0, im:0.0}, duration1), SAMPLE_RATE / 2.0, duration1);
    let next_freq = add_signals(&lfo2hz40amp, &constant_signal(num::Complex{re: 200.0, im: 0.0}, duration1));
    //sigs.push(sine_signal(&constant_signal(num::Complex{re: 1.0, im:0.0}, duration1), &next_freq, duration1));

    
    //sigs.push(shifting_sine(500., 125., 1.0, duration0));
    
    // sigs.push(freq_envelope((SAMPLE_RATE / 2.) as usize, (SAMPLE_RATE as usize) * 2, (SAMPLE_RATE as usize) * 2,
    //                         (SAMPLE_RATE / 2.) as usize,
    //                         800., 50., 150., 800., 1.));
    
    //sigs.push(fm_sine(per0, duration1, ^that thing));
    
    let sig_a = fm_sine(per0, duration0, &shifting_sine(50.0, 150.0, 1.0, duration0));
    let mut sig_b = sig_a.clone();
    sig_b.reverse();
    //sigs.push(sig_a);
    //sigs.push(sig_b);

    let mut scenes = Vec::with_capacity(sigs.len());
    //read a bunch of 16 bit 44100 Hz wav files
    //scenes.push(read_signals_from_wav("scene_1.wav", 16, None));
    for sine_sig0 in sigs {
        scenes.push((sine_sig0.clone(), sine_sig0));
    }
    ///////////////////////////////////
    // Sequence Composition
    ///////////////////////////////////
    println!("Sequence Composition");

    for &mut (ref mut left_sig, ref mut right_sig) in scenes.iter_mut() {
        renormalize_track(left_sig, right_sig);
    }

    let mut left_chan : Signal = Vec::new();
    let mut right_chan : Signal = Vec::new();

    //each scene in order
    for &(ref left_sig, ref right_sig) in scenes.iter() {
        left_chan.extend(left_sig);
        right_chan.extend(right_sig);
    }

    left_chan.extend(vec![num::Complex{re: 0.0, im: 0.0}; WINDOW_SIZE]);
    right_chan.extend(vec![num::Complex{re: 0.0, im: 0.0}; WINDOW_SIZE]);
    //this is the dry composition
    write_wav_from_signals("composition.wav", &left_chan, &right_chan);

    // ///////////////////////////////////
    // // Short Time Fourier Transform Signals
    // ///////////////////////////////////
    // println!("Short Time Fourier Transform Signals");
    // //compute best fft_length
    // let min_fft_len = WINDOW_SIZE + left_wav_sig.len();
    // let mut fft_len: usize = 2;
    // while fft_len < min_fft_len {
    //     fft_len = fft_len * 2;
    // }
    // let window_jump = (WINDOW_SIZE - 1)/2; //50% overlap


    // let input_len = left_chan.len();
    // let mut left_spec = stft(&left_chan, window, window_jump, fft_len);
    // let mut right_spec = stft(&right_chan, window, window_jump, fft_len);

    // ///////////////////////////////////
    // // Filter Signals
    // ///////////////////////////////////
    // println!("Filter Signals");

    // //Fourier transform the kernel
    // let mut left_kernel_sig = vec![num::Complex{re: 0.0, im: 0.0}; fft_len];
    // let mut right_kernel_sig = vec![num::Complex{re: 0.0, im: 0.0}; fft_len];
    // for i in 0..left_wav_sig.len() {
    //     left_kernel_sig[i] = left_wav_sig[i];
    // }
    // for i in 0..right_wav_sig.len() {
    //     right_kernel_sig[i] = right_wav_sig[i];
    // }
    // let mut left_kernel_spec = left_kernel_sig.clone();
    // let mut right_kernel_spec = right_kernel_sig.clone();
    // let mut kernel_fft: rustfft::FFT<f64> = rustfft::FFT::new(fft_len, false);
    // kernel_fft.process(&left_kernel_sig, &mut left_kernel_spec);
    // kernel_fft.process(&right_kernel_sig, &mut right_kernel_spec);
    
    // let interp_end = left_spec.len()-1;
    // for i in 0..left_spec.len() {
    //     let t: f64 = i as f64 / interp_end as f64;
    //     for j in 0..fft_len {
    //         let start_filter = left_kernel_spec[j] * 1.0;
    //         let end_filter = left_kernel_spec[j] * 1.0;
    //         left_spec[i][j] = left_spec[i][j] * (t * end_filter + (1.0 - t) * start_filter);
    //     }
    // }
    // for i in 0..right_spec.len() {
    //     let t: f64 = i as f64 / interp_end as f64;
    //     for j in 0..fft_len {
    //         let start_filter = left_kernel_spec[j] * 1.0; //set start_filter = 1.0 to interpolate between dry and reverb
    //         let end_filter = right_kernel_spec[j] * 1.0;
    //         right_spec[i][j] = right_spec[i][j] * (t * end_filter + (1.0 - t) * start_filter);
    //     }
    // }

    // ///////////////////////////////////
    // // Reverse STFT Signals
    // ///////////////////////////////////
    // println!("Reverse STFT Signals");
    // let left_transformed_sigs = reverse_stft(&left_spec, fft_len);
    // let right_transformed_sigs = reverse_stft(&right_spec, fft_len);

    // ///////////////////////////////////
    // // Overlap-Add to Reconstruct
    // ///////////////////////////////////
    // println!("Overlap-Add to Reconstruct");
    // let mut left_new_sig = vec![num::Complex{re: 0.0, im: 0.0}; input_len];
    // left_new_sig.extend(vec![num::Complex{re: 0.0, im: 0.0}; fft_len]);
    // let mut right_new_sig = vec![num::Complex{re: 0.0, im: 0.0}; input_len];
    // right_new_sig.extend(vec![num::Complex{re: 0.0, im: 0.0}; fft_len]);
    // //overlap-add
    // for i in 0..left_transformed_sigs.len() {
    //     for j in 0..left_transformed_sigs[i].len() {
    //         let temp = left_new_sig[j + (i * window_jump)];
    //         left_new_sig[j + (i * window_jump)] = temp + left_transformed_sigs[i][j];
    //     }
    // }
    // for i in 0..right_transformed_sigs.len() {
    //     for j in 0..right_transformed_sigs[i].len() {
    //         let temp = right_new_sig[j + (i * window_jump)];
    //         right_new_sig[j + (i * window_jump)] = temp + right_transformed_sigs[i][j];
    //     }
    // }

    // ///////////////////////////////////
    // // Renormalize
    // ///////////////////////////////////
    // println!("Renormalize");
    // renormalize_track(&mut left_new_sig, &mut right_new_sig);

    // ///////////////////////////////////
    // // Output .WAV File
    // ///////////////////////////////////
    // println!("Output .WAV File");
    // write_wav_from_signals("output.wav", &left_new_sig, &right_new_sig);

    // ///////////////////////////////////
    // // Play Signals with PortAudio
    // ///////////////////////////////////
    // println!("Play Signals...");
    // let pa = try!(pa::PortAudio::new());
    // let mut settings = try!(pa.default_output_stream_settings(CHANNELS, SAMPLE_RATE, FRAMES_PER_BUFFER));

    // //clipping shouldn't be needed but maybe later
    // settings.flags = pa::stream_flags::NO_FLAG;

    // let mut phase = 0;
    // let new_callback = move |pa::OutputStreamCallbackArgs { buffer, frames, ..}| {
    //     let mut idx = 0;
    //     for _ in 0..frames {
    //         buffer[idx] = left_new_sig[phase].re as f32;
    //         buffer[idx+1] = right_new_sig[phase].re as f32;
    //         phase += 1;
    //         if phase >= left_new_sig.len() { phase -= left_new_sig.len(); }
    //         idx += 2;
    //     }
    //     pa::Continue
    // };

    // let mut new_stream = try!(pa.open_non_blocking_stream(settings, new_callback));

    // try!(new_stream.start());

    // println!("Play OLA signal for {} seconds.", NUM_SECONDS);
    // pa.sleep(NUM_SECONDS * 1_000);
    // try!(new_stream.stop());
    // try!(new_stream.close());
    // println!("OLA TEST FINISHED.");
    
    Ok(())
}
